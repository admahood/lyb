#hmsc figures
library(tidyverse)
library(Hmsc)
# explanatory vs predictive power for all spp vs those with 2 or more occurrences

hmsc_file_all <- "data/hmsc/hmsc_probit_all_every_sp.Rda"
hmsc_file_prev <- "data/hmsc/hmsc_probit_all.Rda"
hmsc_file_b_prev <- "data/hmsc/hmsc_probit.Rda"
hmsc_file_b_all <- "data/hmsc/hmsc_probit_all_app.Rda"

cv_file_all <- "data/hmsc/cv_binomial_all_every_sp.Rda"
cv_file_prev <- "data/hmsc/cv_binomial_all.Rda"
cv_file_b_prev <- "data/hmsc/cv_binomial_burned_prev.Rda"
cv_file_b_all <- "data/hmsc/cv_binomial_burned_all.Rda"

load(hmsc_file_all); m_a<-m
load(hmsc_file_b_prev); m_bp<-m
load(hmsc_file_b_all); m_ba<-m
load(hmsc_file_prev)
load(cv_file_b_all); predYCV_ba<- predYCV
load(cv_file_b_prev); predYCV_bp<- predYCV
load(cv_file_all); predYCV_a<- predYCV
load(cv_file_prev)

preds <- computePredictedValues(m)
preds_a <- computePredictedValues(m_a)
preds_ba <- computePredictedValues(m_ba)
preds_bp <- computePredictedValues(m_bp)

df_l<-list()

df_l$MF = evaluateModelFit(hM=m, predY=preds) %>% as.data.frame %>%
  mutate(model = "prev_spp", 
         power = "explanatory")
df_l$MF_a = evaluateModelFit(hM=m_a, predY=preds_a) %>% as.data.frame %>%
  mutate(model = "all_spp", 
         power= "explanatory")
df_l$MF_ba = evaluateModelFit(hM=m_ba, predY=preds_ba) %>% as.data.frame %>%
  mutate(model = "burned_all_spp", 
         power= "explanatory")
df_l$MF_bp = evaluateModelFit(hM=m_bp, predY=preds_bp) %>% as.data.frame %>%
  mutate(model = "burned_prev_spp", 
         power= "explanatory")

df_l$MFCV = evaluateModelFit(hM=m, predY=predYCV)%>% as.data.frame %>%
  mutate(model = "prev_spp", power = "predictive")
df_l$MFCV_a = evaluateModelFit(hM=m_a, predY=predYCV_a)%>% as.data.frame %>%
  mutate(model = "all_spp", power = "predictive")
df_l$MFCV_ba = evaluateModelFit(hM=m_ba, predY=predYCV_ba)%>% as.data.frame %>%
  mutate(model = "burned_all_spp", power = "predictive")
df_l$MFCV_bp = evaluateModelFit(hM=m_bp, predY=predYCV_bp)%>% as.data.frame %>%
  mutate(model = "burned_prev_spp", power = "predictive")

df<- bind_rows(df_l)

ggplot(df,aes(x=model, y=RMSE, fill = power)) + geom_boxplot()
ggplot(df,aes(x=model, y=AUC, fill = power)) + geom_boxplot()
ggplot(df,aes(x=model, y=TjurR2, fill = power)) + geom_boxplot()
