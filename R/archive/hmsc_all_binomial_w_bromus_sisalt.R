# Hmsc burned and unburned with b_tectorum as a predictor

# setup ========================================================================
source("R/a_data_prep.R")

library(knitr)
library(ape)
library(MASS)
library(fields)
library(Hmsc)
library(parallel)
library(ggthemes)
library(ggtext)
library(corrplot)
library(ggpubr)
set.seed(1)
theme_set(theme_classic())


# data wrangling ===============================================================
# veg community
Y <- veg_spread %>%
  dplyr::select(-shrub_pq) %>%
  arrange(plot) %>%
  tibble::column_to_rownames("plot") %>%
  as.matrix
Y[Y>0] <-1

# Y<- Y[,colSums(Y)>1] # removing species that only occur in one location

# colnames(C) <- str_replace_all(colnames(C), " ", "_")
prevalence<- colSums(Y) %>%
  as_tibble(rownames = "Species") %>%
  dplyr::rename(prevalence = value)

coords <-  sites %>%
  arrange(plot) %>%
  st_coordinates %>%
  as.data.frame()  %>%
  dplyr::rename("x-coordinate" = X, "y-coordinate" = Y)

#env data

XData <- sites_w_grazing %>%
  arrange(plot) %>%
  dplyr::select(watershed, elevation_m, folded_aspect,plot,
                grazing_intensity, burned, B_tectorum) %>%
  mutate(watershed = as.factor(watershed),
         burned=as.factor(burned))%>%
  tibble::column_to_rownames("plot") %>%
  as.data.frame %>%
  mutate("y-coordinate" = coords[,1],
         "x-coordinate" = coords[,2])


#species traits
traits <- as.data.frame(veg_traits) %>%
  filter(species %in% colnames(Y))%>%
  dplyr::select(-fg) %>%
  transmute_all((as.factor)) %>%
  tibble::column_to_rownames("species")


tr_form <- ~origin+duration+cots

XFormula <- ~elevation_m+
  folded_aspect+
  grazing_intensity+
  burned+
  B_tectorum

studyDesign <- data.frame(watershed = as.factor(XData$watershed))
studyDesign <- data.frame(sample = as.factor(1:nrow(XData)))

rL = HmscRandomLevel(units = studyDesign$watershed)
rLs = HmscRandomLevel(sData = coords%>% as.matrix()) %>%
  setPriors(nfMin=1, nfMax=1)

# fitting the model ============================================================
m = Hmsc(Y = Y, XData = XData, XFormula = XFormula,distr="probit",
         TrData = traits,TrFormula = tr_form,
         studyDesign = studyDesign, ranLevels = list(sample = rLs))

nChains = 4
test.run = FALSE
if (test.run){
  #with this option, the vignette evaluates in ca. 1 minute in adam's laptop
  thin = 1
  samples = 100
  transient = ceiling(thin*samples*.5)
}else{
  # with a spatial random effect, evaluates in ---
  # looks like a compute-optimized aws instance is called for, very little ram usage
  thin = 100
  samples = 1000
  transient = ceiling(thin*samples*.5)
}
t0 <- Sys.time()
hmsc_file <- "data/hmsc/hmsc_probit_all_every_sp_w_bromus.Rda"
dir.create("data/hmsc")
if(!file.exists(hmsc_file)){
  m = sampleMcmc(m, thin = thin, 
                 samples = samples, 
                 transient = transient,
                 adaptNf = rep(ceiling(0.4*samples*thin),1),
                 nChains = nChains, 
                 nParallel = nChains)
  print(Sys.time()-t0)
  save(m, file=hmsc_file)
  system(paste("aws s3 cp",
               hmsc_file,
               file.path("s3://earthlab-amahood/lyb", hmsc_file)))
}else{load(hmsc_file)}


# mcmc convergence ===================
mpost <- convertToCodaObject(m)
psrf.V = gelman.diag(mpost$V,multivariate=FALSE)$psrf%>%
  as_tibble() %>% dplyr::rename(psrf_v = `Point est.`)


ess.beta <- effectiveSize(mpost$Beta) %>%
  as_tibble() %>% dplyr::rename(ess_beta = value)

ess.v <- effectiveSize(mpost$V)%>%
  as_tibble() %>% dplyr::rename(ess_v = value)
psrf.beta <- gelman.diag(mpost$Beta, multivariate=FALSE)$psrf%>%
  as_tibble() %>% dplyr::rename(psrf_beta = `Point est.`)

# ess.gamma = effectiveSize(mpost$Gamma)%>%
#   as_tibble() %>% rename(ess_gamma = value)
# psrf.gamma = gelman.diag(mpost$Gamma, multivariate=FALSE)$psrf%>%
#   as_tibble() %>% rename(psrf_gamma = `Point est.`)
# 
# sppairs = matrix(sample(x = 1:ns^2, size = 100))
# tmp = mpost$Omega[[1]]
# for (chain in 1:length(tmp)){
#   tmp[[chain]] = tmp[[chain]][,sppairs]
# }
# 
# ess.omega = effectiveSize(tmp)%>%
#   as_tibble() %>% rename(ess_omega = value)
# psrf.omega = gelman.diag(tmp, multivariate=FALSE)$psrf%>%
#   as_tibble() %>% rename(psrf_omega = `Point est.`)

ggarrange(ggplot(ess.beta, aes(x=ess_beta)) + geom_histogram(),
          ggplot(ess.v, aes(x=ess_v)) + geom_histogram(),
          #ggplot(ess.omega, aes(x=ess_omega)) + geom_histogram(),
          ggplot(psrf.beta, aes(x=psrf_beta)) + geom_histogram(),
          ggplot(psrf.V, aes(x=psrf_v)) + geom_histogram(),
          # ggplot(psrf.gamma, aes(x=psrf_gamma)) + geom_histogram(),
          # ggplot(psrf.omega, aes(x=psrf_omega)) + geom_histogram(),
          align = "v") +
  ggsave("images/geldman_allplots_wbrom.png", width = 3.5, height=3.5, bg="white")

# fit stuff ===========

# explanatory power
preds = computePredictedValues(m)
MF = evaluateModelFit(hM=m, predY=preds)

ggplot(as.data.frame(MF),aes(x=(RMSE))) + geom_histogram()
ggplot(as.data.frame(MF),aes(x=(TjurR2))) + geom_histogram()
ggplot(as.data.frame(MF),aes(x=(AUC))) + geom_histogram()

# predictive power

partition = createPartition(m, nfolds = 10) # takes 2.5 hrs with 10 folds
cv_file <- "data/hmsc/cv_binomial_all_every_sp.Rda"
# cv_file <- "data/hmsc/cv_binomial_all.Rda"

if(!file.exists(cv_file)){
  t0<- Sys.time()
  print(t0)
  predYCV = computePredictedValues(m, partition = partition, nParallel = 2)
  save(predYCV, file = cv_file)
  print(Sys.time()-t0)
}else{load(cv_file)}
# Note that in computePredicted values it is also possible to use the nParallel option
# Below we construct a plot that compares explanatory power (MF) to predictive power (MFCV)
# As expected, the explanatory power is higher than the predictive power

MFCV = evaluateModelFit(hM=m, predY=predYCV)
plot(MF$AUC, MFCV$AUC)
abline(0,1)


# plotting variance partitioning ===============================================

mf_df <- data.frame(Species = colnames(m$Y),
                    R2 = MF$TjurR2,
                    AUC = MF$AUC,
                    RMSE = MF$RMSE)
VP <- computeVariancePartitioning(m)
# VP$R2T$Beta
# VP$R2T$Y


sbquants <- summary(mpost$Beta)$quantiles %>%
  as_tibble(rownames = "variable") %>% 
  mutate(sign = `2.5%` * `97.5%`) %>%
  filter(sign>0) %>%
  separate(variable,
           into = c("variable", "species"),
           sep = ",") %>%
  mutate(variable = str_sub(variable, 3,nchar(variable)-5),
         species = str_sub(species, 2,nchar(species)-6) %>% trimws) %>%
  filter(variable!= "(Intercept)") %>%
  dplyr::select(variable,species,`2.5%`,`50%`,`97.5%`) %>%
  arrange(variable)


vp_df <- VP$vals%>%
  as_tibble(rownames = "variable") %>%
  pivot_longer(cols=names(.)[2:ncol(.)], 
               names_to = "Species", 
               values_to = "value") %>%
  left_join(mf_df)

vp_order_n <- vp_df %>%
  filter(variable == "Random: sample") %>%
  left_join(traits %>% rownames_to_column("Species")) %>%
  left_join(prevalence) %>%
  filter(origin=="N") %>%
  arrange(prevalence, origin) %>%
  mutate(Species_f = factor(Species, levels = .$Species)) %>%
  dplyr::select(Species, Species_f, origin)


vp_order <- vp_df %>% filter(variable == "Random: sample") %>%
  left_join(traits %>% rownames_to_column("Species")) %>%
  filter(origin=="I") %>%
  left_join(prevalence) %>%
  arrange(prevalence, origin) %>%
  mutate(Species_f = factor(Species, levels = .$Species)) %>%
  dplyr::select(Species, Species_f, origin) %>%
  rbind(vp_order_n) %>%
  left_join(mf_df)

cols<-vector()
cols[1] <- "#FED439"
cols[2] <- RColorBrewer::brewer.pal(12, "Paired")[c(12)]
cols[3:5] <-  RColorBrewer::brewer.pal(3, "Blues")
cols[6]<- "grey"

left_join(vp_df, vp_order) %>%
  mutate(variable = factor(variable, 
                           levels = c("elevation_m",
                                      "folded_aspect",
                                      "grazing_intensity", 
                                      "burnedyes", 
                                      "B_tectorum",
                                      "Random: sample"),
                           labels = c("Elevation (m)",
                                      "Folded Aspect",
                                      "Grazing Intensity", 
                                      "Burned", 
                                      "B. tectorum cover",
                                      "Random: Spatial")),
         value = value) %>%
  ggplot(aes(x=value,y=reorder(Species_f,Species), fill = variable)) +
  geom_bar(stat="identity")+
  theme_classic() +
  geom_hline(yintercept = table(vp_order$origin)[1]+.5) +
  geom_hline(yintercept = nrow(vp_order)+.5) +
  annotate("text", x = 1.6, y=1, label="Introduced", angle=90, vjust="bottom",
           hjust="left", size=8)+
  annotate("text", x = 1.6, y=nrow(vp_order), label="Native", angle=90, vjust="top",
           hjust="right", size=8)+
  ylab("Species") +
  xlab("Proportion of Variance Explained") +
  scale_fill_manual(values = cols)+
  theme(legend.position = c(1,.315),
        legend.title = element_blank(),
        legend.justification = c(1,0),
        legend.background = element_rect(color="black")) +
  ggtitle("Variance Partitioning, Occurrence Model")+
  ggsave("images/variance_partitioning_binomial.png", height = 10.5, width = 7)


# table(veg_traits$origin)[1]

# species niches ...basically ==================================================

postBeta <- getPostEstimate(m, parName = "Beta")

means <- postBeta$mean %>%
  as_tibble() %>%
  rowid_to_column("env_var") %>%
  mutate(env_var = c("intercept",VP$groupnames)) %>%
  pivot_longer(cols=names(.)[2:ncol(.)], names_to = "Species", values_to = "Mean")


supported <- postBeta$support %>% 
  as_tibble() %>%
  rowid_to_column("env_var") %>%
  mutate(env_var = c("intercept",VP$groupnames)) %>%
  pivot_longer(cols=names(.)[2:ncol(.)], 
               names_to = "Species", 
               values_to = "Support") %>%
  filter(Support >0.90|Support<0.10,
         env_var != "intercept") %>%
  left_join(means, by = c("env_var", "Species"))%>%
  mutate(sign = ifelse(Mean>0, "+", "-"))%>%
  mutate(env_var = factor(env_var, 
                          levels = c("elevation_m",
                                     "folded_aspect",
                                     "grazing_intensity", 
                                     "burnedyes", 
                                     "B_tectorum",
                                     "Random: sample"),
                          labels = c("Elevation (m)",
                                     "Folded Aspect",
                                     "Grazing Intensity", 
                                     "Burned", 
                                     "*B. tectorum* cover",
                                     "Random: Spatial"))) %>%
  left_join(vp_order) 

line_df <- supported %>%
  group_by(Species) %>%
  dplyr::summarise(origin = first(origin)) %>%
  ungroup()

p_betas_all_plots<-ggplot(supported, aes(x=env_var, y = Species_f, fill = Mean, color = sign)) +
  geom_tile(lwd=.5) +
  theme_clean()+
  scale_fill_gradient2(mid = "grey90", name="Mean\nEffect\nSize") +
  scale_color_manual(values = c(("red"), ("blue"))) +
  geom_hline(yintercept = table(line_df$origin)[1] + .5) +
  annotate("text", x = 5.75, y=1, label="Introduced", angle=90, vjust="bottom",
           hjust="left", size=6)+
  annotate("text", x = 5.75, y=nrow(line_df), label="Native", angle=90, vjust="bottom",
           hjust="right", size=6)+
  guides(color = "none")+
  theme(axis.text.x = element_markdown(angle=45, vjust=1,hjust = 1),
        axis.title = element_blank()) +
  ggsave("images/betas_binomial_all_spp.png")

save(p_betas_all_plots, file = "data/p_beta_all_plots.Rda")

plotBeta(m, post = postBeta, param = "Support",
         supportLevel = 0.95, split=.4, spNamesNumbers = c(T,F))
plotBeta(m, post = postBeta,param = "Mean",
         supportLevel = 0.95, split=.4, spNamesNumbers = c(T,F))
postGamma = getPostEstimate(m, parName = "Gamma")
plotGamma(m, post=postGamma)
plotGamma(m, post=postGamma, param="Support", supportLevel = 0.95)



# gradient grazing intensity ==========

Gradient = constructGradient(m, focalVariable = "grazing_intensity",
                             non.focalVariables=list(elevation_m=list(1),
                                                     burned=list(1),
                                                     folded_aspect = list(1)))
Gradient$XDataNew$burned <-2

predY = predict(m, XData=Gradient$XDataNew, studyDesign=Gradient$studyDesignNew, 
                ranLevels=Gradient$rLNew, expected=TRUE)

n_runs <- nChains*samples

pred_df <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(grazing_intensity = rep(Gradient$XDataNew$grazing_intensity, n_runs)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -grazing_intensity) %>%
  group_by(Species, grazing_intensity) %>%
  dplyr::summarise(mean = median(cover),
                   upr = quantile(cover, 0.975),
                   lwr = quantile(cover, 0.025)) %>%
  ungroup() %>%
  # mutate(Species = str_replace_all(Species," ", "_"),
  #        Species = lut_prevalent_species[Species]) %>%
  left_join(prevalence) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  filter(origin != "IPG" & origin != "IW") %>%
  arrange(desc(prevalence)) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species))) %>%
  filter(Species != "unknown_forb") 

pred_df_raw <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(grazing_intensity = rep(Gradient$XDataNew$grazing_intensity, n_runs),
         run = rep(1:n_runs,each=20)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -c(grazing_intensity,run))%>%
  # mutate(Species = str_replace_all(Species," ", "_")) %>%
  left_join(prevalence) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  arrange(origin,desc(prevalence)) %>%
  filter(Species != "unknown_forb")%>%
  # mutate(Species = lut_prevalent_species[Species]) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species)))

pred_df_raw  %>%
  filter(origin != "IPG" & origin != "IW") %>%
  mutate(origin = c("IAF" = "Introduced Annual Forb",
                    "IAG" = "Introduced Annual Grass",
                    "NAF" = "Native Annual Forb",
                    "NPF" = "Native Perennial Forb",
                    "NPG" = "Native Perennial Grass",
                    "NW" = "Native Shrub")[origin])%>%
  ggplot(aes(x=grazing_intensity, y=cover)) +
  geom_line(alpha=0.05, aes(group=run, color = origin), key_glyph="rect")+
  geom_line(data = pred_df,lwd=1, alpha=0.95, color="black", aes(y=mean))+
  facet_wrap(~Species_f, nrow=4)+
  xlab("Grazing Intensity (AUM/ha)") +
  ylab("Probability of Occurrence") +
  guides(color=guide_legend(override.aes = list(alpha=1)))+
  scale_color_brewer(palette = "Dark2") +
  # scale_color_manual(values = c("#FFC845", "#007DBA"), 
  #                    labels = c("Introduced", "Native"))+
  theme(legend.position = c(1,0),
        legend.justification = c(1,0),
        legend.title = element_blank())+
  ggsave("images/probit_preds_grazing_intensity_byspp_all.png", width=13.5, height=9.5)

# gradient burned ==================
m$XData$burned <- as.factor(m$XData$burned)
Gradient = constructGradient(m, focalVariable = "burned",
                             non.focalVariables=list(elevation_m=list(1),
                                                     grazing_intensity=list(1),
                                                     folded_aspect = list(1)))

predY = predict(m, XData=Gradient$XDataNew, studyDesign=Gradient$studyDesignNew, 
                ranLevels=Gradient$rLNew, expected=TRUE)


n_runs <- nChains*samples


pred_df <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(burned = rep(Gradient$XDataNew$burned, n_runs)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -burned) %>%
  group_by(Species, burned) %>%
  dplyr::summarise(mean = median(cover),
                   upr = quantile(cover, 0.975),
                   lwr = quantile(cover, 0.025)) %>%
  ungroup() %>%
  left_join(prevalence)%>%
  mutate(origin = lut_all_fg[Species]) %>%
  filter(origin != "IPG" & origin != "IW") %>%
  arrange(origin,desc(prevalence)) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species))) %>%
  filter(Species != "unknown forb") 

pred_df_raw <-do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(burned = rep(Gradient$XDataNew$burned, n_runs),
         run = rep(1:n_runs,each=2)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -c(burned,run))%>%
  left_join(prevalence)%>%
  mutate(origin = lut_all_fg[Species]) %>%
  filter(origin != "IPG" & origin != "IW") %>%
  arrange(origin, desc(prevalence)) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species))) %>%
  filter(Species != "unknown forb")  %>%
  mutate(origin = c("IAF" = "Introduced Annual Forb",
                    "IAG" = "Introduced Annual Grass",
                    "NAF" = "Native Annual Forb",
                    "NPF" = "Native Perennial Forb",
                    "NPG" = "Native Perennial Grass",
                    "NW" = "Native Shrub")[origin])

pred_df_raw  %>%
  mutate(burned = ifelse(burned == "yes", "B", "U")%>%
           factor(levels = c("U", "B"))) %>%
  ggplot(aes(x=burned, y=cover)) +
  geom_jitter(alpha=0.04, aes(color = origin))+
  geom_boxplot(fill = "transparent", outlier.shape = NA)+
  facet_wrap(~Species_f, nrow=4)+
  xlab("Burned") +
  ylab("Probability of Occurrence") +
  guides(color=guide_legend(override.aes = list(alpha=1)))+
  scale_color_brewer(palette = "Dark2") +
  theme(legend.position = c(1,0),
        legend.justification = c(1,0),
        legend.background = element_rect(fill="transparent"),
        legend.title = element_blank())+
  ggsave("images/probit_preds_all_burned.png", width=10.5, height=6.5)


