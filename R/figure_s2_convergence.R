library(tidyverse)
library(gghmsc)
library(coda)
library(Hmsc)
library(ggpubr)
load('data/r1_iga_convergence.rda')
load('data/hmsc/r1_hmsc_probit_burned_subplot_grp_Feb_20_1p5M.Rda')
load('data/hmsc/latest_all_subplot_grp.Rda')

mpost <- Hmsc::convertToCodaObject(m)
mbpost <- Hmsc::convertToCodaObject(mb)

gelman_ma <- coda::gelman.diag(mpost$Beta)
ess_ma <- coda::effectiveSize(mpost$Beta)

gelman_ma$psrf %>% as_tibble() |> janitor::clean_names() |> mutate(s=ifelse(point_est >1.1, 'over', 'under')) |> pull(s) |>table()

p_gelman_iga <- ggplot(gelman_d$psrf %>% as_tibble) +
  geom_histogram(aes(x=`Point est.`))+
  xlab("Gelman Diagnostic") +
  ggthemes::theme_clean() +
  ggtitle("IGA Convergence")
p_ess_iga <- ggplot(ess %>% as_tibble) +
  geom_histogram(aes(x=value))+
  xlab("Effective Sample Size") +
  ggthemes::theme_clean() +
  xlim(c(0,NA))

p_gelman_ma <- ggplot(gelman_ma$psrf %>% as_tibble) +
  geom_histogram(aes(x=`Point est.`))+
  geom_label(x=1.2, y=30, label = "23 of 168 paramters over 1.1") +
  xlab("Gelman Diagnostic") +
  ggthemes::theme_clean() +
  ggtitle("JSDM (All Plots) Convergence")
p_ess_ma <- ggplot(ess_ma %>% as_tibble) +
  geom_histogram(aes(x=value))+
  xlab("Effective Sample Size") +
  ggthemes::theme_clean() +
  xlim(c(0,NA))

p1 <- ggarrange(p_gelman_iga,p_gelman_ma,p_ess_iga,p_ess_ma)

gelman_mb <- coda::gelman.diag(mbpost$Beta)
ess_mb <- coda::effectiveSize(mbpost$Beta)
gelman_mbg <- coda::gelman.diag(mbpost$Gamma)
ess_mbg <- coda::effectiveSize(mbpost$Gamma)

p2 <- gghmsc::gghm_convergence(mb, beta =T, gamma = T, omega = T) +
  ggthemes::theme_clean() +
  ggtitle("Model Convergence: JSDM (Burned Plots)")

ggarrange(p1, p2) -> pfinal

ggsave('images/convergence_all3_mods.png', width = 15, height = 8)
