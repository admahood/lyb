# figs for the lyb study
# Many of these are blatantly copied from the supplemental material in the
# Steven Walker et al. 2015 study cited in the paper 
# source("R/iga_burned_subplot.R")
source("R/a_data_prep.R")
library(ggpubr)
library(ggtext)
library(tidyverse)
# load("data/mcmc_b/cors.Rda")

comm <- veg_burned_subplot %>%
  arrange(plot, subplot) %>%
  mutate(plot = paste0(plot, subplot)) %>%
  dplyr::select(-subplot) %>%
  column_to_rownames("plot")
Y <- 1*(as.matrix(comm) > 0)

Y <- Y[,colSums(Y) > 1] # remove species present in only one site
Y <- Y[,colSums(Y) < nrow(Y)] # remove species present in every site
n <- nrow(Y) # number of sites
m <- ncol(Y) # number of species

# reorder data
siteOrder <- order(as.numeric(rownames(Y)))
Y <- Y[siteOrder, ]

if(!file.exists("data/posts_bs.rda")){
  postMean_flipped <- lapply(postMean,function(x)x*-1)
  postCI_flipped <- lapply(postCI_hdi,function(x)x*-1)
  save(postMean_flipped, postCI_flipped,postMean, postCI, 
       file = "data/posts_bs.rda")}else{
  load("data/posts_bs.rda")
}

# correlations with the gradient for species and sites =========================
lut_origin<- c( "Agropyron cristatum" = "Introduced", 
                "Alyssum desertorum"="Introduced", 
                "Amsinkia intermedia" ="Native",
                "Artemisia tridentata ssp. wyomingensis"="Native",
                "Artemisia tridentata ssp"="Native",
                
                "Astragalus whitneyi"                   ="Native",
                "Microsteris gracilis" = "Native",
                "Bassia prostrata"                      ="Introduced",
                "Bassia scoparia" = "Introduced",
                "Medicago sativa" = "Introduced",
                "Hordeum murinum" = "Introduced",
                "Collinsia parviflora" = "Native",
                "Atriplex confertifolia" = "Native",
                "Chenopodiaceae sp." = "Introduced",
                "Chenopodiaceae sp" = "Introduced",
                
                "Calochortus bruneaunis"                ="Native"
                , "Cardaria draba"                        ="Introduced"
                , "Ceratocephala testiculata"             ="Introduced"
                , "cf Phlox sp. (unk 9)"                  ="Native"
                , "Chrysothamnus viscidiflorus"           ="Native"
                , "Crepis occidentalis"                   ="Native"
                , "Descurainia pinnata"                   ="Native"
                , "Descurainia sophia"                    ="Introduced"
                , "Draba albertina"                       ="Native"
                , "Elymus cinereus"                       ="Native"
                , "Elymus elymoides"                      ="Native"
                , "Ericameria nauseosa"                   ="Native"
                , "Erodium cicutarium"                 ="Introduced"
                , "Grayia spinosa"                ="Native"  
                , "Lagophylla glandulosa" = "Native"
                , "Lactuca serriola"         ="Introduced"         
                , "Lepidium perfoliatum"="Introduced"              
                , "Phlox longifolia"     ="Native"                 
                , "Poa secunda" = "Native"                  
                , "Salsola tragus" = "Introduced"                     
                , "Sisymbrium altissimum" = "Introduced"             
                , "Sphaeralcea coccinea" = "Native"                  
                , "Sphaeralcea parvifolia" = "Native"              
                , "Stipa comata" = "Native"                 
                , "Stipa hymenoides" = "Native"                      
                ,"Tetradymia glabrata" = "Native"             
                ,"Tragopogon dubius" = "Introduced"                
                ,"Vulpia bromoides" = "Introduced"              
                , "Zigadenus sp." = "Native")

dfSpeciesOrd <- data.frame(species = names(postMean$b),
                           mean = postMean$b,
                           low = postCI$b[1, ],
                           up = postCI$b[2, ])%>%
  mutate(origin = lut_origin[species],
         species = replace(species, 
                           species =="Artemisia tridentata ssp", 
                           "Artemisia tridentata"))
dfSpeciesOrd <- within(dfSpeciesOrd, species <- factor(species, species[order(mean)]))

priorPrecision <- 5
# species
pspec <- ggplot(dfSpeciesOrd, aes(color = origin)) + 
  theme_pubclean() +
  geom_vline(xintercept = qnorm(c(0.025, 0.975), 
                                sd = sqrt(1/priorPrecision)),
             alpha = 0.7) +
  geom_vline(xintercept = 0, lty = 2)+
  scale_color_brewer(palette = "Set1") +
  geom_linerange(aes(y = species, xmin = low, xmax = up), 
                 lwd =2,key_glyph="rect") +
  scale_size_continuous(range = c(0.1, 3)) +
  theme(legend.title = element_blank(),
        legend.position.inside = c(0,1),
        axis.text.y = element_text(face = 'italic'),
        legend.justification = c(0,1),
        legend.background = element_rect("transparent")) +
  labs(y = "Species", x = "Slope parameter");pspec 

ggsave("images/species_ord_walker_b_subplot.png", pspec, height =5, width=5)

# sites
if(!file.exists("data/species_site_ord_dfs_bs.rda")){
  dfSiteOrd <- data.frame(subplot = row.names(Y),
                          mean = postMean$x,
                          low = postCI$x[1, ],
                          up = postCI$x[2, ]) %>%
    mutate(plot = str_sub(subplot,1, nchar(subplot)-1)) %>% 
    left_join(site_data_b)
  
  dfSiteOrd <- within(dfSiteOrd, subplot <- factor(subplot, subplot[order(mean)]))
  
  save(dfSpeciesOrd, dfSiteOrd, file = "data/species_site_ord_dfs_bs.rda")
  }else(load("data/species_site_ord_dfs_bs.rda"))

psite <- ggplot(dfSiteOrd) + 
  theme_classic() +
  geom_vline(xintercept = qnorm(c(0.025, 0.975), sd = 1),
             alpha = 0.7, lty=2) +
  geom_vline(lty = 1, xintercept = 0) +
  geom_linerange(aes(y = subplot, xmin = low, xmax = up
                     ,color = B_tectorum
                     ), lwd=.65, alpha = 0.75
                 ) +
  scale_color_viridis_c(name = "Cheatgrass Cover")+
  scale_size_continuous(range = c(0.1, 3), name = "Cheatgrass Cover") +
  labs(y = "Sites (Subplots)", x = "Gradient") + 
  theme(legend.position = c(1,0),
        axis.text.y = element_blank(),
        axis.ticks = element_blank(),
        legend.justification = c(1,0),
        legend.background = element_rect(color = "black", fill = "white"));psite

ggsave("images/site_ord_walker_b_subplot.png",psite, height=10, width=10)

# correlations with env and climate ============================================

lut_variables<-c("plot"="plot",               
                 "subplot" = "subplot",
                 "def_z_month_of_fire" = "CWD",         
                 "max_def_z_preceeding_fire" = "CWD (Max) Before",
                 "median_def_z_preceeding_fire" = "CWD (Med) Before",
                 "min_def_z_preceeding_fire" = "CWD (Min) Before",
                 "median_def_z_preceeding_fire" = "CWD (Med) Before",
                 "median_def_z_after_fire" = "CWD (Med) After",
                 "max_def_z_after_fire" = "CWD (Max) After",
                 "median_def_z_after_fire" = "CWD (Med) After",    
                 "min_def_z_after_fire" = "CWD (Min) After",
                 "tmn_z_month_of_fire" = "Minimum Temperature (Month of)",
                 "max_tmn_z_preceeding_fire" = "Minimum Temp (Max) Before",
                 "median_tmn_z_preceeding_fire" = "Minimum Temp (Med) Before",
                 "min_tmn_z_preceeding_fire" = "Minimum Temp (Min) Before",
                 "max_tmn_z_after_fire" = "Minimum Temp (Max) After",
                 "median_tmn_z_after_fire" = "Minimum Temp (Med) After",
                 "min_tmn_z_after_fire" = "Minimum Temp (Min) After",
                 "aet_z_month_of_fire" = "AET (Month of)",
                 "max_aet_z_preceeding_fire" = "AET (Max) Before",  
                 "median_aet_z_preceeding_fire" = "AET (Med) Before",
                 "min_aet_z_preceeding_fire" = "AET (Min) Before",
                 "max_aet_z_after_fire" = "AET (Max) After",
                 "median_aet_z_after_fire" = "AET (Med) After",
                 "min_aet_z_after_fire" = "AET (Min) After",
                 "tsf" = "Time Since Fire",           
                 "B_tectorum" = "Cheatgrass Cover",
                 "Folded_aspect" = "Folded Aspect",
                 "Shrub_cover" = "Shrub Cover",
                 "Month" = "Ignition Month",
                 "Elevation" = "Elevation",
                 "Shannon" = "Shannon Diversity",
                 "Pielou" = "Pielou's Evenness",
                 "Grazing" = "Grazing Intensity")


load("data/cors_subplot_bs.rda")
if(!file.exists("data/cors_df.rda")){
  during_df <- do.call("cbind", cor_clim) %>%
    gather(key = Variable, value=value) %>%
    mutate(keep = str_detect(Variable, "month_of")) %>%
    filter(keep == TRUE) %>%
    mutate(Variable = lut_variables[Variable],
           prime_var = str_sub(Variable, 1,3),
           prime_var = ifelse(prime_var == "Min", "Tmn", prime_var))
  before_df <- do.call("cbind", cor_clim) %>%
    gather(key = Variable, value=value) %>%
    mutate(keep = str_detect(Variable, "preceeding")) %>%
    filter(keep == TRUE)%>%
    mutate(Variable = lut_variables[Variable],
           prime_var = str_sub(Variable, 1,3),
           prime_var = ifelse(prime_var == "Min", "Tmn", prime_var))
  after_df <- do.call("cbind", cor_clim) %>%
    gather(key = Variable, value=value) %>%
    mutate(keep = str_detect(Variable, "after")) %>%
    filter(keep == TRUE)%>%
    mutate(Variable = lut_variables[Variable])
  
  cors_df <- do.call("cbind", cor_list)%>%
    gather(key = Variable, value = value) %>%
    mutate(Variable = lut_variables[Variable]) %>%
    bind_rows(before_df %>%
                # filter(Variable == "AET (Min)" |
                #        Variable == "Minimum Temp (Med)")%>%
                # mutate(Variable = c("AET (Min)"="Prefire AET",
                #                     "Minimum Temp (Med)" = "Prefire T<sub>min</sub>")[Variable]) |>
                dplyr::select(-keep, -prime_var)) %>%
    bind_rows(after_df %>%
                # filter(Variable == "CWD (Max)")%>%
                # mutate(Variable = "Postfire CWD") |>
                dplyr::select(-keep))
  
  cors_dford<- cors_df %>%
    dplyr::group_by(Variable) %>%
    dplyr::summarise(mean = mean(value)) %>%
    ungroup()%>%
    arrange((mean)) %>%
    mutate(Variable_f = factor(Variable, levels = .$Variable))
  
  cors_df <- left_join(cors_df, cors_dford)
  save(cors_df, file="data/cors_df.rda", compress = TRUE)

}else{load("data/cors_df.rda")}


# climate horizontal bars ============

pden_hb_climate_full <- ggplot(cors_df) +
  ggdist::stat_halfeye(aes(y = as.factor(Variable_f), x=value)) +
  theme_pubclean() +
  geom_vline(xintercept = 0, lty=4, color= "grey") +
  xlab("Correlation with the Gradient") +
  theme(axis.title.y = element_blank(),
        axis.text.y = ggtext::element_markdown())

# only with significant climate variables
strongvars <- cors_df |>
  as_tibble() |>
  # pull(Variable) |> unique()
  dplyr::group_by(Variable) |>
  dplyr::summarise(mean = mean(value),
            sd = sd(value)) |>
  ungroup() |>
  dplyr::filter(abs(mean) > sd*1.96) |>
  pull(Variable)

pden_df <- cors_df |>
  dplyr::select(Variable, value, mean) |>
  filter(Variable %in% strongvars,
         !str_detect(Variable, "Min"), 
         !str_detect(Variable, "Max")) |>
  mutate(Variable = str_remove_all(Variable,'\\(Med\\) '))
  
pden_dford<- pden_df %>%
  dplyr::group_by(Variable) %>%
  dplyr::summarise(mean = mean(value)) %>%
  ungroup()%>%
  arrange((mean)) %>%
  mutate(Variable_f = factor(Variable, levels = .$Variable))


 pden <-  pden_df |>
   left_join(pden_dford) |>
   ggplot() +
  ggdist::stat_halfeye(aes(y = as.factor(Variable_f), x=value)) +
  theme_pubclean() +
  geom_vline(xintercept = 0, lty=4, color= "grey") +
  xlab("Correlation with the Gradient") +
  theme(axis.title.y = element_blank(),
        axis.text.y = ggtext::element_markdown())

multiplot <- ggarrange(psite, pspec, pden,
                       labels="auto", ncol=2, nrow=2)

ggsave("images/iga_bs.png", multiplot,
       height = 8, width=8, bg="white")



