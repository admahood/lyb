# lyb study
# hmsc burned and unburned

source("R/a_data_prep.R")

library(Hmsc)
library(ggtext)
library(parallel)
library(ggthemes)
library(ggpubr)
set.seed(1)
theme_set(theme_classic())
dir.create("data/hmsc")
corz <- parallel::detectCores()-2

# look up tables ===============================================================

lut_grp <- c("Agoseris glauca"  = "other NPF",
             "Agoseris heterophylla" = "other NPF",
             "Agropyron cristatum" = "Agropyron cristatum",
             "Allium sp. (unk 1)"  = "other NPF",
             "Alyssum desertorum" = "Alyssum desertorum",
             "Amsinkia intermedia"    = "Amsinkia intermedia",
             "Artemisia arbusculus" = "other NW",
             "Artemisia tridentata ssp. wyomingensis" = "A. t. wyomingensis",
             "Asteraceae sp"   = "other NPF",
             "Astragalus lentiginosus"  = "other NLF",
             "Astragalus sp."  = "other NLF",
             "Astragalus sp. (ukas)"  = "other NLF",
             "Astragalus sp. (unk 8)" = "other NLF",
             "Astragalus whitneyi" = "other NLF",
             "Atriplex confertifolia"  = "other NW",
             "Bassia prostrata" = "Bassia prostrata",
             "Bassia scoparia"  = "other IAF",
             "Bromus tectorum" = "Bromus tectorum",
             "Calochortus bruneaunis" = "other NPF",
             "Cardaria draba" = "other IPF",
             "Ceratocephala testiculata" = "Ceratocephala testiculata",
             "cf. Atriplex sp." = "other NW",
             'cf Phlox sp. (unk 9)' = "Microsteris gracilis",
             "cf. Macraenthera sp." = "other NPF",
             "Chaenactis douglasii" = "other NPF",
             "Chenopodiaceae sp." = "other NAF",
             "Chrysothamnus viscidiflorus" = "other NW",
             "Collinsia parviflora" = "other NAF",
             "Convolvulus sp." = "other IPF",
             "Crepis occidentalis" = "other NPF",
             "Descurainia pinnata" = "other NPF",
             "Descurainia sophia" = "other IAF",
             "Draba albertina" = "other NAF",
             "Elymus cinereus" = "other NPG",
             "Elymus elymoides" = "Elymus elymoides",
             "Ephedra nevadaensis" = "other NW",
             "Ericameria nauseosa" = "Ericameria nauseosa",
             "Eriogonum sp."  = "other NPF",
             "Erodium cicutarium" = "Erodium cicutarium",
             "Gayophytum ramosissimum" = "other NAF",
             "Grayia spinosa"   = "other NW",
             "Halogetan glomeratus" = "other IAF",
             "Hordeum murinum" = "other IAG",
             "Lactuca serriola" = "other IAF",
             "Lagophylla glandulosa" = "other NAF",
             "Layia glandulosa" = "other NAF",
             "Lepidium perfoliatum" = "Lepidium perfoliatum",
             "Lupinus argenteus" = "other NLF",
             "Lupinus lepidus" = "other NLF",
             "Medicago sativa" = "other IPF",
             "Microsteris gracilis" =  "Microsteris gracilis",
             "Mirabilis albida" = "other NPF",
             "Oenothera sp." = "other NPF",
             "Opuntia sp."  = "Opuntia_sp",
             "Penstemon sp" = "other NPF",
             "Perideridia bolanderi" = "other NPF",
             "Phlox diffusa" = "other NPF",
             "Phlox hoodii" = "other NPF",
             "Phlox longifolia" = "other NPF",
             "Poa secunda" = "Poa secunda",
             "Salsola tragus" = "Salsola tragus",
             "Sarcobatus maniculatus"  = "other NW",
             "Sisymbrium altissimum"  = "Sisymbrium altissimum",
             "Sphaeralcea coccinea" = "Sphaeralcea_spp",
             "Sphaeralcea parvifolia" = "Sphaeralcea_spp",
             "Stephanomeria pauciflora" = "other NPF",
             "Stipa comata" = "other NPG",
             "Stipa hymenoides" = "other NPG",
             "Tetradymia glabrata" = "other NW",
             "Tetradymia spinosa" = "other NW",
             "Tragopogon dubius" = "other IAF",
             "Unknown perennial grass" = "other NPG",
             "Vulpia bromoides" = "other IAG" ,                     
             "Zigadenus sp." = "other NPF")

# burned data prep =============================================================
# veg community

veg_burned_subplot <- dplyr::select(veg_all_subplot,-fg)%>%
  left_join(site_data %>% st_set_geometry(NULL)%>% dplyr::select(plot, burned)) %>%
  filter(burned=="yes")%>%
  dplyr::select(-burned) %>% 
  mutate(species = lut_grp[species]) %>%
  group_by(plot, subplot, species) %>%
  dplyr::summarise(cover = sum(cover)) %>%
  ungroup() %>%
  spread(species, cover, fill=0)

Yb <- veg_burned_subplot %>%
  mutate(plotsub = paste0(plot, "_", subplot)) %>%
  dplyr::arrange(plotsub) %>%
  tibble::column_to_rownames("plotsub") %>%
  dplyr::select(-subplot, -plot) %>%
  as.matrix
Yb[Yb>0] <-1


prevalenceb<- colSums(Yb) %>%
  as_tibble(rownames = "Species") %>%
  dplyr::rename(prevalence = value) %>%
  arrange(desc(prevalence))

#env data
clim <- read_csv("data/lyb_climate_z.csv") %>%
  dplyr::select(-`...1`) %>%
  filter(variable == "min_def_z_after_fire" |
           variable == "min_def_z_preceeding_fire"|
           variable == "median_aet_z_after_fire"
  ) %>%
  pivot_wider(id_cols = plot, names_from = "variable", values_from = value)

coordsb <-  sites %>%
  filter(burned == "yes") %>%
  arrange(plot) %>%
  st_coordinates %>%
  as.data.frame()  %>%
  dplyr::rename("x-coordinate" = X, "y-coordinate" = Y)

XDatab <- sites_w_grazing %>%
  filter(burned == "yes") %>%
  left_join(clim)%>%
  # st_set_geometry(NULL)%>%
  arrange(plot) %>%
  dplyr::select(watershed, elevation_m, folded_aspect,plot,
                grazing_intensity, tsf, min_def_z_after_fire,
                min_def_z_preceeding_fire, StartMonth,
                median_aet_z_after_fire, B_tectorum, shrub_pq) %>%
  mutate(watershed = as.factor(watershed))%>%
  as.data.frame %>%
  mutate("y-coordinate" = coordsb[,1],
         "x-coordinate" = coordsb[,2])%>%
  left_join(x = dplyr::select(veg_burned_subplot,plot, subplot) %>% unique(), 
            y = .) %>%
  mutate(plotsub = paste(plot, subplot, sep="_")) %>%
  tibble::remove_rownames() %>%
  tibble::column_to_rownames("plotsub") |>
  left_join(fg |> janitor::clean_names())  %>%
  mutate_if(is.character, as.factor) 

summary(XDatab)


#species traits
traitsb <- as.data.frame(veg_traits_b) %>%
  mutate(species = lut_grp[species]) %>%
  filter(species %in% colnames(Yb))%>%
  transmute_all((as.factor)) %>%
  # mutate(perennial = ifelse(duration == "P"| duration == "W", "yes", "no"),
  #        annual = ifelse(duration == "A", "yes", "no"),
  #        woody = ifelse(duration == "W", "yes", "no"),
  #        legume = ifelse(species == "native_legume_forb", "yes", "no")) %>%
  group_by(species) %>%
  summarise_all(first) %>%
  ungroup() %>%
  tibble::column_to_rownames("species") 

tr_form <- ~ fg

XFormulab <- ~elevation_m+
  folded_aspect+
  grazing_intensity+
  StartMonth+
  tsf+
  min_def_z_after_fire+
  min_def_z_preceeding_fire+
  median_aet_z_after_fire  +
  B_tectorum+
  shrub_pq +
  perennial_herbaceous

studyDesignb <- data.frame(watershed = as.factor(XDatab$watershed),
                           plot = as.factor(XDatab$plot),
                           subplot = as.factor(XDatab$subplot))

# rLb = HmscRandomLevel(xData = data.frame(plot = studyDesignb$plot, 
#                                         # subplot = studyDesignb$subplot,
#                                         watershed = studyDesignb$watershed))

rLb = HmscRandomLevel(units = studyDesignb$plot)

# fitting the models ===========================================================

modb = Hmsc(Y = Yb, XData = XDatab, XFormula = XFormulab, distr="probit",
            TrData = traitsb, TrFormula = tr_form,
            studyDesign = studyDesignb, ranLevels = list("plot" = rLb))

nChains = 4
test.run = FALSE
if (test.run){
  #with this option, the vignette evaluates in ca. 1 minute in adam's laptop
  thin = 1
  samples = 10
  transient = ceiling(thin*samples*.5)
}else{
  #with this option, the vignette evaluates in ca. 4 hours in adam's laptop
  # with a spatial random effect, evaluates in ---
  thin = 1000
  samples = 1000
  transient = ceiling(thin*samples*.5)
}

t_iter <- str_c(((thin * samples) + transient)/1e6, "M") |>
  str_replace_all("\\.", "p")
day <- date() %>% 
  str_replace_all("  ", " ") %>%
  str_split(" ", simplify = TRUE)

# burned

hmsc_fileb <- str_c("data/hmsc/r1_hmsc_probit_burned_subplot_grp_",
                    day[2],"_",day[3],"_", t_iter,".Rda")
hmsc_fileb1 <- "data/hmsc/r1_latest_burned_subplot_grp.Rda"

if(!file.exists(hmsc_fileb)){
  t0 <- Sys.time()
  mb = sampleMcmc(modb, thin = thin, 
                  samples = samples, 
                  transient = transient,
                  adaptNf = rep(ceiling(0.4*samples*thin),1),
                  nChains = nChains, 
                  nParallel = corz)
  print(Sys.time()-t0)
  save(mb, file=hmsc_fileb) # save twice, once with date, once with most recent version
  save(mb, file=hmsc_fileb1) 
  
}else{load(hmsc_fileb1)}


# plotting ==============
library(gghmsc)

load('data/hmsc/r1_hmsc_probit_burned_subplot_grp_Feb_21_0p75M.Rda')

gghm_convergence(mb, gamma = T, omega = T)
gghm_gamma(mb, support_level = 0.95) +
  xlab('Environmental Covariates') +
  ggtitle("Effects on Plant Functional Groups")
gghm_beta(mb)
gghm_omega(mb)
gghm_beta2(mb)


