# setup ========================================================================
source("R/a_data_prep.R")

library(knitr)
library(ape)
library(MASS)
library(fields)
library(Hmsc)
library(ggtext)
library(parallel)
library(ggthemes)
library(corrplot)
library(ggpubr)
set.seed(1)
theme_set(theme_classic())

# TO DO - add height as a trait

lut_grp <- c("Agoseris glauca"  = "native_forb",
             "Agoseris heterophylla" = "native_forb",
             "Agropyron cristatum" = "Agropyron cristatum",
             "Allium sp. (unk 1)"  = "native_forb",
             "Alyssum desertorum" = "Alyssum desertorum",
             "Amsinkia intermedia"    = "Amsinkia intermedia",
             "Artemisia arbusculus" = "native_shrub",
             "Artemisia tridentata ssp. wyomingensis" = "Artemisia tridentata ssp. wyomingensis",
             "Asteraceae sp"   = "native_forb",
             "Astragalus lentiginosus"  = "native_legume_forb",
             "Astragalus sp."  = "native_legume_forb",
             "Astragalus sp. (ukas)"  = "native_legume_forb",
             "Astragalus sp. (unk 8)" = "native_legume_forb",
             "Astragalus whitneyi" = "native_legume_forb",
             "Atriplex confertifolia"  = "native_shrub",
             "Bassia prostrata" = "Bassia prostrata",
             "Bassia scoparia"  = "introduced_forb",
             "Bromus tectorum" = "Bromus tectorum",
             "Calochortus bruneaunis" = "native_forb",
             "Cardaria draba" = "introduced_forb",
             "Ceratocephala testiculata" = "Ceratocephala testiculata",
             "cf. Atriplex sp." = "native_shrub",
             "cf. Macraenthera sp." = "native_forb",
             "Chaenactis douglasii" = "native_forb",
             "Chenopodiaceae sp." = "native_forb",
             "Chrysothamnus viscidiflorus" = "native_shrub",
             "Collinsia parviflora" = "native_forb",
             "Convolvulus sp." = "introduced_forb",
             "Crepis occidentalis" = "native_forb",
             "Descurainia pinnata" = "native_forb",
             "Descurainia sophia" = "introduced_forb",
             "Draba albertina" = "native_forb",
             "Elymus cinereus" = "native_perennial_graminoid",
             "Elymus elymoides" = "Elymus elymoides",
             "Ephedra nevadaensis" = "native_shrub",
             "Ericameria nauseosa" = "Ericameria nauseosa",
             "Eriogonum sp."  = "native_forb",
             "Erodium cicutarium" = "Erodium cicutarium",
             "Gayophytum ramosissimum" = "native_forb",
             "Grayia spinosa"   = "native_shrub",
             "Halogetan glomeratus" = "introduced_forb",
             "Hordeum murinum" = "introduced_annual_graminoid",
             "Lactuca serriola" = "introduced_forb",
             "Lagophylla glandulosa" = "native_forb",
             "Layia glandulosa" = "native_forb",
             "Lepidium perfoliatum" = "Lepidium perfoliatum",
             "Lupinus argenteus" = "native_legume_forb",
             "Lupinus lepidus" = "native_legume_forb",
             "Medicago sativa" = "Medicago sativa",
             "Microsteris gracilis" =  "Microsteris gracilis",
             "Mirabilis albida" = "native_forb",
             "Oenothera sp." = "native_forb",
             "Opuntia sp."  = "Opuntia_sp",
             "Penstemon sp" = "native_forb",
             "Perideridia bolanderi" = "native_forb",
             "Phlox diffusa" = "native_forb",
             "Phlox hoodii" = "native_forb",
             "Phlox longifolia" = "native_forb",
             "Poa secunda" = "Poa secunda",
             "Salsola tragus" = "Salsola tragus",
             "Sarcobatus maniculatus"  = "native_shrub",
             "Sisymbrium altissimum"  = "Sisymbrium altissimum",
             "Sphaeralcea coccinea" = "Sphaeralcea_spp",
             "Sphaeralcea parvifolia" = "Sphaeralcea_spp",
             "Stephanomeria pauciflora" = "native_forb",
             "Stipa comata" = "native_perennial_graminoid",
             "Stipa hymenoides" = "native_perennial_graminoid",
             "Tetradymia glabrata" = "native_shrub",
             "Tetradymia spinosa" = "native_shrub",
             "Tragopogon dubius" = "introduced_forb",
             "Unknown perennial grass" = "native_perennial_graminoid",
             "Vulpia bromoides" = "introduced_annual_graminoid" ,                     
             "Zigadenus sp." = "native_forb")

# data wrangling ===============================================================
# veg community

veg_burned_subplot <- dplyr::select(veg_all_subplot,-fg)%>%
  left_join(site_data %>% st_set_geometry(NULL)%>% dplyr::select(plot, burned)) %>%
  filter(burned=="yes")%>%
  dplyr::select(-burned) %>%
  mutate(species = lut_grp[species]) %>%
  group_by(plot, subplot, species) %>%
  summarise(cover = sum(cover)) %>%
  ungroup() %>%
  spread(species, cover, fill=0)

Y <- veg_burned_subplot %>%
  mutate(plotsub = paste0(plot, "_", subplot)) %>%
  dplyr::arrange(plotsub) %>%
  tibble::column_to_rownames("plotsub") %>%
  dplyr::select(-subplot, -plot) %>%
  as.matrix
Y[Y>0] <-1

# Y<- Y[,colSums(Y)>1]

# colnames(C) <- str_replace_all(colnames(C), " ", "_")
prevalence<- colSums(Y) %>%
  as_tibble(rownames = "Species") %>%
  dplyr::rename(prevalence = value) %>%
  arrange(desc(prevalence))

#env data
clim <- read_csv("data/lyb_climate_z.csv") %>%
  dplyr::select(-`...1`) %>%
  filter(variable == "max_def_z_after_fire" |
           variable == "median_tmn_z_preceeding_fire"|
           variable == "min_aet_z_preceeding_fire") %>%
  pivot_wider(id_cols = plot, names_from = "variable", values_from = value)

coords <-  sites %>%
  filter(burned == "yes") %>%
  arrange(plot) %>%
  st_coordinates %>%
  as.data.frame()  %>%
  dplyr::rename("x-coordinate" = X, "y-coordinate" = Y)

XData <- sites_w_grazing %>%
  filter(burned == "yes") %>%
  left_join(clim)%>%
  # st_set_geometry(NULL)%>%
  arrange(plot) %>%
  dplyr::select(watershed, elevation_m, folded_aspect,plot,
                grazing_intensity, tsf,max_def_z_after_fire,
                median_tmn_z_preceeding_fire, StartMonth,
                min_aet_z_preceeding_fire, B_tectorum) %>%
  mutate(watershed = as.factor(watershed))%>%
  as.data.frame %>%
  mutate("y-coordinate" = coords[,1],
         "x-coordinate" = coords[,2])%>%
  left_join(x = dplyr::select(veg_burned_subplot,plot, subplot) %>% unique(), 
            y = .) %>%
  mutate(plotsub = paste(plot, subplot, sep="_")) %>%
  tibble::remove_rownames() %>%
  tibble::column_to_rownames("plotsub") %>%
  mutate_if(is.character, as.factor)


#species traits
traits <- as.data.frame(veg_traits_b) %>%
  mutate(species = lut_grp[species]) %>%
  filter(species %in% colnames(Y))%>%
  transmute_all((as.factor)) %>%
  mutate(perennial = ifelse(duration == "P"| duration == "W", "yes", "no"),
         annual = ifelse(duration == "A", "yes", "no"),
         woody = ifelse(duration == "W", "yes", "no"),
         legume = ifelse(species == "native_legume_forb", "yes", "no")) %>%
  group_by(species) %>%
  summarise_all(first) %>%
  ungroup() %>%
  tibble::column_to_rownames("species") 

# add legumes
tr_form <- ~origin+annual+perennial+woody+cots+legume

XFormula <- ~elevation_m+
  folded_aspect+
  grazing_intensity+
  StartMonth+
  tsf+
  max_def_z_after_fire+
  median_tmn_z_preceeding_fire+
  min_aet_z_preceeding_fire +
  B_tectorum

studyDesign <- data.frame(watershed = as.factor(XData$watershed),
                          plot = as.factor(XData$plot),
                          subplot = as.factor(XData$subplot))

rL = HmscRandomLevel(xData = data.frame(plot = studyDesign$plot, 
                                        subplot = studyDesign$subplot,
                                        watershed = studyDesign$watershed))

rL = HmscRandomLevel(units = studyDesign$plot)
# rL$nfMax = 15

# fitting the model ============================================================
m = Hmsc(Y = Y, XData = XData, XFormula = XFormula,distr="probit",
         TrData = traits,TrFormula = tr_form,
         studyDesign = studyDesign, ranLevels = list("plot" = rL))

nChains = 4
test.run = FALSE
if (test.run){
  #with this option, the vignette evaluates in ca. 1 minute in adam's laptop
  thin = 1
  samples = 10
  transient = ceiling(thin*samples*.5)
}else{
  #with this option, the vignette evaluates in ca. 4 hours in adam's laptop
  # with a spatial random effect, evaluates in ---
  thin = 100
  samples = 1000
  transient = ceiling(thin*samples*.5)
}
t0 <- Sys.time()
# hmsc_file <- "data/hmsc/hmsc_probit.Rda"
# hmsc_file <- "data/hmsc/hmsc_probit_all_app.Rda"
day<- date()%>% str_split(" ", simplify = TRUE)

hmsc_file <- str_c("data/hmsc/hmsc_probit_burned_all_spp_spatial_w_bromus_grp",
                   day[2], day[3],".Rda")
dir.create("data/hmsc")

# hmsc_file <- "data/hmsc/hmsc_probit_burned_all_spp_spatial_w_bromus_grpAug25.Rda"

if(!file.exists(hmsc_file)){
  m = sampleMcmc(m, thin = thin, 
                 samples = samples, 
                 transient = transient,
                 adaptNf = rep(ceiling(0.4*samples*thin),1),
                 nChains = nChains, 
                 nParallel = nChains)
  print(Sys.time()-t0)
  save(m, file=hmsc_file) # save twice, once with date, once with most recent version

}else{load(hmsc_file)}

mpost <- convertToCodaObject(m)
preds <- computePredictedValues(m)
MF <- evaluateModelFit(hM=m, predY=preds)

# mcmc convergence ===================
psrf.V = gelman.diag(mpost$V,multivariate=FALSE)$psrf%>%
  as_tibble() %>% dplyr::rename(psrf_v = `Point est.`)


ess.beta_b <- effectiveSize(mpost$Beta) %>%
  as_tibble() %>% dplyr::rename(ess_beta = value)

ess.v <- effectiveSize(mpost$V)%>%
  as_tibble() %>% dplyr::rename(ess_v = value)
psrf.beta_b<- gelman.diag(mpost$Beta, multivariate=FALSE)$psrf%>%
  as_tibble() %>% dplyr::rename(psrf_beta = `Point est.`)

ess.gamma = effectiveSize(mpost$Gamma)%>%
  as_tibble() %>% rename(ess_gamma = value)
psrf.gamma = gelman.diag(mpost$Gamma, multivariate=FALSE)$psrf%>%
  as_tibble() %>% rename(psrf_gamma = `Point est.`)

# sppairs = matrix(sample(x = 1:ns^2, size = 100))
# tmp = mpost$Omega[[1]]
# for (chain in 1:length(tmp)){
#   tmp[[chain]] = tmp[[chain]][,sppairs]
# }
# 
# ess.omega = effectiveSize(tmp)%>%
#   as_tibble() %>% rename(ess_omega = value)
# psrf.omega = gelman.diag(tmp, multivariate=FALSE)$psrf%>%
#   as_tibble() %>% rename(psrf_omega = `Point est.`)

diag_b<-ggarrange(ggplot(ess.beta_b, aes(x=ess_beta)) + 
                    geom_histogram()+
                    xlab("Effective Sample Size"),
                  ggplot(psrf.beta_b, aes(x=psrf_beta)) +
                    geom_histogram()+
                    xlab("Gelman Diagnostic"),
          align = "v") + ggtitle("Burned Plots")

ggsave(diag_b, filename = "images/geldman_burned_probit.png", width = 3.5, height=3.5, bg="white")
save(ess.beta_b, psrf.beta_b, file = "data/diag_b.Rda")
# fit stuff ===========

# explanatory power
mean(MF$TjurR2, na.rm=T)

ggarrange(
ggplot(as.data.frame(MF),aes(x=(RMSE))) + geom_histogram(),
ggplot(as.data.frame(MF),aes(x=(TjurR2))) + geom_histogram(),
ggplot(as.data.frame(MF),aes(x=(AUC))) + geom_histogram())

# predictive power
# 
# partition = createPartition(m, nfolds = 10)
# # cv_file <- "data/hmsc/cv_binomial_burned_prev.Rda"
# cv_file <- "data/hmsc/cv_binomial_burned_all.Rda"
# 
# if(!file.exists(cv_file)){
#   t0<- Sys.time()
#   print(t0)
#   predYCV = computePredictedValues(m, partition = partition, nParallel = 2)
#   save(predYCV, file = cv_file)
#   print(Sys.time()-t0)
# }else{load(cv_file)}
# Note that in computePredicted values it is also possible to use the nParallel option
# Below we construct a plot that compares explanatory power (MF) to predictive power (MFCV)
# As expected, the explanatory power is higher than the predictive power

# MFCV = evaluateModelFit(hM=m, predY=predYCV)
# plot(MF$AUC, MFCV$AUC)
# abline(0,1)


# plotting variance partitioning ===============================================

mf_df <- data.frame(Species = colnames(m$Y),
                    R2 = MF$TjurR2,
                    AUC = MF$AUC,
                    RMSE = MF$RMSE)
VP <- computeVariancePartitioning(m)
# VP$R2T$Beta
# VP$R2T$Y


sbquants <- summary(mpost$Beta)$quantiles %>%
  as_tibble(rownames = "variable") %>% 
  mutate(sign = `2.5%` * `97.5%`) %>%
  filter(sign>0) %>%
  separate(variable,
           into = c("variable", "species"),
           sep = ",") %>%
  mutate(variable = str_sub(variable, 3,nchar(variable)-5),
         species = str_sub(species, 2,nchar(species)-6) %>% trimws) %>%
  filter(variable!= "(Intercept)") %>%
  dplyr::select(variable,species,`2.5%`,`50%`,`97.5%`) %>%
  arrange(variable)


vp_df <- VP$vals%>%
  as_tibble(rownames = "variable") %>%
  pivot_longer(cols=names(.)[2:ncol(.)], 
               names_to = "Species", 
               values_to = "value") %>%
  left_join(mf_df)

vp_order_n <- vp_df %>%
  filter(variable == "Random: plot") %>%
  left_join(traits %>% rownames_to_column("Species")) %>%
  filter(origin=="N") %>%
  left_join(prevalence) %>%
  mutate(duration = str_replace_all(duration,"C", "W")) %>%
  arrange(duration, cots, prevalence) %>%
  mutate(Species_f = factor(Species, levels = .$Species)) %>%
  dplyr::select(Species, Species_f, origin)


vp_order <- vp_df %>% filter(variable == "Random: plot") %>%
  left_join(traits %>% rownames_to_column("Species")) %>%
  filter(origin=="I") %>%
  left_join(prevalence) %>%
  arrange(duration, cots, prevalence) %>%
  mutate(Species_f = factor(Species, levels = .$Species)) %>%
  dplyr::select(Species, Species_f, origin) %>%
  rbind(vp_order_n) %>%
  left_join(mf_df)

cols<-vector()
cols[1] <- "#FED439"
cols[2] <- RColorBrewer::brewer.pal(12, "Paired")[c(12)]
cols[3:6] <- RColorBrewer::brewer.pal(4, "OrRd")
cols[7:9] <-  RColorBrewer::brewer.pal(3, "Blues")
cols[10]<- "grey"

p_vp_b <-left_join(vp_df, vp_order) %>%
  mutate(variable = factor(variable, 
                           levels = c("elevation_m",
                                      "folded_aspect",
                                     "StartMonth",
                                      "grazing_intensity", 
                                      "B_tectorum",
                                      "tsf",
                                      "max_def_z_after_fire",
                                      "median_tmn_z_preceeding_fire",
                                      "min_aet_z_preceeding_fire", 
                                      "Random: plot"),
                           labels = c("Elevation (m)",
                                      "Folded Aspect",
                                      "Ignition Month",
                                      "Grazing Intensity", 
                                      "Bromus tectorum cover",
                                      "Time Since Fire",
                                      "Post-fire Dryness",
                                      "Pre-fire Warm Nights",
                                      "Pre-fire Wetness", 
                                      "Random: plot")),
         value = value) %>%
  ggplot(aes(x=value,y=reorder(Species_f,Species), fill = variable)) +
  geom_bar(stat="identity")+
  theme_classic() +
  geom_hline(yintercept = table(vp_order$origin)[1]+.5) +
  geom_hline(yintercept = nrow(vp_order)+.5) +
  annotate("text", x = 1.3, y=1, label="Introduced", angle=90, vjust="bottom",
           hjust="left", size=8)+
  annotate("text", x = 1.3, y=nrow(vp_order), label="Native", angle=90, vjust="top",
           hjust="right", size=8)+
  ylab("Species") +
  xlab("Proportion of Variance Explained") +
  scale_fill_manual(values = cols)+
  theme(legend.position = c(1,.315),
        legend.title = element_blank(),
        legend.justification = c(1,0),
        legend.background = element_rect(color="black")) +
  ggtitle("Variance Partitioning, Burned Plots")

ggsave(p_vp_b,filename= "images/variance_partitioning_binomial_burned.png", height = 10.5, width = 10)


# table(veg_traits$origin)[1]

# species niches ...basically ==================================================

postBeta <- getPostEstimate(m, parName = "Beta")

means <- postBeta$mean %>%
  as_tibble() %>%
  rowid_to_column("env_var") %>%
  mutate(env_var = c("intercept",VP$groupnames)) %>%
  pivot_longer(cols=names(.)[2:ncol(.)], names_to = "Species", values_to = "Mean")


supported <- postBeta$support %>% 
  as_tibble() %>%
  rowid_to_column("env_var") %>%
  mutate(env_var = c("intercept",VP$groupnames)) %>%
  pivot_longer(cols=names(.)[2:ncol(.)], 
               names_to = "Species", 
               values_to = "Support") %>%
  filter(Support >0.89|Support<0.11,
         env_var != "intercept") %>%
  left_join(means, by = c("env_var", "Species"))%>%
  mutate(sign = ifelse(Mean>0, "+", "-"))%>%
  mutate(env_var = factor(env_var, 
                           levels = c("elevation_m",
                                      "folded_aspect",
                                      "StartMonth",
                                      "grazing_intensity", 
                                      "B_tectorum",
                                      "tsf",
                                      "max_def_z_after_fire",
                                      "median_tmn_z_preceeding_fire",
                                      "min_aet_z_preceeding_fire", 
                                      "Random: plot"),
                           labels = c("Elevation",
                                      "Folded Aspect",
                                      "Ignition Month",
                                      "Grazing Intensity",
                                      "*B. tectorum* cover",
                                      "Time Since Fire",
                                      "Post-fire CWD",
                                      "Pre-fire T<sub>min</sub>",
                                      "Pre-fire AET", 
                                      "Random: plot"))) %>%
  left_join(vp_order) %>%
  filter(Species != "Bassia prostrata", 
         Species != "Agropyron cristatum",
         Species != "Opuntia sp.",
         Species != "Unknown perennial grass") %>%
  left_join(traits %>% tibble::rownames_to_column("Species")) %>%
  mutate(fg = paste0(origin, duration,"_", cots))

line_df <- supported %>%
  group_by(Species) %>%
  dplyr::summarise(origin = first(origin)) %>%
  ungroup()

load("data/p_beta_all_plots_subplot_grps.Rda")

supported_gg <-
  supported_all %>%
  filter(env_var == "Burned") %>%
  mutate(env_var = ifelse(env_var=="Burned", 
                          "Fire Occurrence", 
                          env_var)) %>%
  rbind(supported)

nvars <- unique(supported_gg$env_var) %>% length()

beta_burned_plot<- ggplot(supported_gg, aes(x=env_var, y = Species_f, 
                                         fill = sign)) +
  theme_clean()+
  geom_hline(lwd=15.5,aes(yintercept=Species, color=fg))+
  geom_hline(lty=3, aes(yintercept=Species), color="black")+
  geom_tile(lwd=.5, color = "black")+#, aes(alpha=Mean)) +
  scale_color_brewer(name = "Functional Group", palette = "Accent") +
  # scale_color_manual(values = c(("red"), ("blue"))) +
  # scale_color_colorblind()+
  geom_hline(yintercept = table(line_df$origin)[1] + .5,lwd=1) +
  annotate("text", x = nvars + 1, y=1, label="Introduced", angle=90, vjust="bottom",
           hjust="left", size=7, fontface="bold")+
  annotate("text", x = nvars + 1, y=nrow(line_df)/1.5, label="Native", angle=90, vjust="bottom",
           hjust="right", size=7, fontface="bold")+
  guides(alpha="none")+
  theme(legend.position = "right",#c(1,.315),
        # legend.title = element_blank(),
        # legend.justification = c(1,0),
        legend.background = element_rect(color="black")) +
  theme(axis.text.x = element_markdown(angle=45, vjust=1,hjust = 1,),
        axis.title = element_blank()) 
  # ggtitle("Burned Plots Only")+

ggsave(beta_burned_plot, filename="images/betas_binomial_burned_subplot_grp.png", height=10, width=10)
save(supported, beta_burned_plot, file = "data/beta_burned.Rda")


 
ggarrange(beta_burned_plot, p_betas_all_plots, nrow=1,widths = c(1.1,1),
          common.legend = TRUE, legend="right", labels="AUTO")+
  ggsave(filename = "images/both_betas.png", width = 15, height=10)



# SET UP A TWO PANEL FIG WITH THE ALL PLOTS BETA FIG

postGamma = getPostEstimate(m, parName = "Gamma")
# aet bad for natives, tsf bad for woody, elevation good for woody and monocot
plotGamma(m, post=postGamma, param="Support", supportLevel = 0.895)
plotGamma(m, post=postGamma, param="Mean", supportLevel = 0.890)

# NOTE THIS RESULT AND ADD INTO THE PAPER

# gradient tsf ==========

Gradient = constructGradient(m, focalVariable = "tsf")

predY = predict(m, XData=Gradient$XDataNew, studyDesign=Gradient$studyDesignNew, 
                ranLevels=Gradient$rLNew, expected=TRUE)
# plotGradient(m, Gradient, pred=predY, measure="S", showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 16, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 53, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 55, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 43, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 7, showData = TRUE)
# 
# plotGradient(m, Gradient, pred=predY, measure="T", index = 2, showData = TRUE)


# 
# pred_df<-do.call("rbind", predY) %>%
#   as_tibble() %>%
#   mutate(tsf = rep(Gradient$XDataNew$tsf, 2000)) %>%
#   pivot_longer(values_to = "cover", names_to = "species", -tsf) %>%
#   group_by(species, tsf) %>%
#   dplyr::summarise(mean = median(cover),
#             upr = quantile(cover, 0.975),
#             lwr = quantile(cover, 0.025)) %>%
#   ungroup() %>%
#   mutate(fg = lut_all_fg[species],
#          origin = str_sub(fg, 1,1),
#          fg= lut_gradient[fg])
# 
# veg_pa<-veg_burned[,2:68]
# veg_pa[veg_pa>0] <- 1
# veg_pa$plot <- veg_burned$plot
# obs <- left_join(veg_pa, sites_w_grazing)
# 
# pred_df %>% filter(species == 'Bromus tectorum'|
#                      species == 'Poa secunda'|
#                      species == 'Lepidium perfoliatum'|
#                      species == 'Artemisia tridentata ssp. wyomingensis')%>%
#   mutate(species = replace(species, 
#                            species =="Artemisia tridentata ssp. wyomingensis",
#                            "A. tridentata"))%>%
# ggplot(aes(x=tsf, y= mean)) +
#   geom_line()+
#   geom_line(lty=2, aes(y=upr))+
#   geom_line(lty=2, aes(y=lwr))+
#   facet_wrap(~species,nrow=1)+
#   xlab("Time Since Fire") +
#   ylab("Predicted Probability of Occurrence") +
#   ggsave("images/binomial_preds_tsf.png", width=7, height=3)
# 
# 
# pred_df %>%
#   filter(fg != "NC" & fg != "IW" & fg != "IPG") %>%
# ggplot(aes(x=tsf, y= mean, group=species,color = origin)) +
#   geom_line(lwd=1, alpha=0.5)+
#   facet_wrap(~fg) +
#   xlab("Time Since Fire") +
#   ylab("Probability of Occurrence") +
#   theme(legend.position = c(1,.45),
#         legend.justification = c(1,1))+
#   ggsave("images/fg_preds_tsf_binomial.png", height=7, width=7) 
# 
# ggplot(pred_df, aes(x=tsf, y= mean, group=species,color = origin)) +
#   geom_line(lwd=1, alpha=0.5)+
#    geom_line(lty=2, aes(y=upr))+
#    geom_line(lty=2, aes(y=lwr)) +
#   facet_wrap(~species)+
#   theme(axis.text=element_blank())

# from sb ============
n_runs <- nChains*samples


pred_df <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(tsf = rep(Gradient$XDataNew$tsf, n_runs)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -tsf) %>%
  group_by(Species, tsf) %>%
  dplyr::summarise(mean = median(cover),
                   upr = quantile(cover, 0.975),
                   lwr = quantile(cover, 0.025)) %>%
  ungroup() %>%
  # mutate(Species = str_replace_all(Species," ", "_"),
  #        Species = lut_prevalent_species[Species]) %>%
  left_join(prevalence) %>%
  filter(prevalence>4) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  filter(origin != "IPG" & origin != "IW") %>%
  arrange(desc(prevalence)) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species))) %>%
  filter(Species != "unknown_forb") 

pred_df_raw <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(tsf = rep(Gradient$XDataNew$tsf, n_runs),
         run = rep(1:n_runs,each=20)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -c(tsf,run))%>%
  # mutate(Species = str_replace_all(Species," ", "_")) %>%
  left_join(prevalence) %>%
  filter(prevalence>4) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  arrange(origin,desc(prevalence)) %>%
  filter(Species != "unknown_forb")%>%
  # mutate(Species = lut_prevalent_species[Species]) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species)))

pred_df_raw  %>%
  filter(origin != "IPG" & origin != "IW") %>%
  mutate(origin = c("IAF" = "Introduced Annual Forb",
                    "IAG" = "Introduced Annual Grass",
                    "NAF" = "Native Annual Forb",
                    "NPF" = "Native Perennial Forb",
                    "NPG" = "Native Perennial Grass",
                    "NW" = "Native Shrub")[origin]) %>%
  ggplot(aes(x=tsf, y=cover)) +
  geom_line(alpha=0.01, aes(group=run, color = origin), key_glyph="rect")+
  geom_line(data = pred_df,lwd=1, alpha=0.95, color="black", aes(y=mean))+
  facet_wrap(~Species_f, nrow=4)+
  xlab("Time Since Fire (Years)") +
  ylab("Probability of Occurrence") +
  guides(color=guide_legend(override.aes = list(alpha=1)))+
  scale_color_brewer(palette = "Dark2") +
  # scale_color_manual(values = c("#FFC845", "#007DBA"), 
  #                    labels = c("Introduced", "Native"))+
  theme(legend.position = "right")+
  ggsave("images/probit_preds_tsf_byspp.png", width=13.5, height=9.5)

# gradient grazing intensity ==========

Gradient = constructGradient(m, focalVariable = "grazing_intensity",
                             non.focalVariables=list(elevation_m=list(1),
                                                     max_def_z_after_fire=list(1),
                                                     folded_aspect = list(1),
                                                     tsf=list(1),
                                                     median_tmn_z_preceeding_fire=list(1), 
                                                     StartMonth=list(1),
                                                     min_aet_z_preceeding_fire=list(1)))

predY = predict(m, XData=Gradient$XDataNew, studyDesign=Gradient$studyDesignNew, 
                ranLevels=Gradient$rLNew, expected=TRUE)
# plotGradient(m, Gradient, pred=predY, measure="S", showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 16, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 53, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 55, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 43, showData = TRUE)
# plotGradient(m, Gradient, pred=predY, measure="Y", index = 7, showData = TRUE)
# 
# plotGradient(m, Gradient, pred=predY, measure="T", index = 2, showData = TRUE)

# 
# 
# pred_df<-do.call("rbind", predY) %>%
#   as_tibble() %>%
#   mutate(grazing_intensity = rep(Gradient$XDataNew$grazing_intensity, 2000)) %>%
#   pivot_longer(values_to = "cover", names_to = "species", -grazing_intensity) %>%
#   group_by(species, grazing_intensity) %>%
#   dplyr::summarise(mean = median(cover),
#             upr = quantile(cover, 0.975),
#             lwr = quantile(cover, 0.025)) %>%
#   ungroup() %>%
#   mutate(fg = lut_all_fg[species],
#          origin = str_sub(fg, 1,1),
#          fg= lut_gradient[fg])
# 
# veg_pa<-veg_burned[,2:68]
# veg_pa[veg_pa>0] <- 1
# veg_pa$plot <- veg_burned$plot
# obs <- left_join(veg_pa, sites_w_grazing)
# 
# pred_df %>% filter(species == 'Bromus tectorum'|
#                      species == 'Poa secunda'|
#                      species == 'Lepidium perfoliatum'|
#                      species == 'Artemisia tridentata ssp. wyomingensis')%>%
#   mutate(species = replace(species, 
#                            species =="Artemisia tridentata ssp. wyomingensis",
#                            "A. tridentata"))%>%
#   ggplot(aes(x=grazing_intensity, y= mean)) +
#   geom_line()+
#   geom_line(lty=2, aes(y=upr))+
#   geom_line(lty=2, aes(y=lwr))+
#   facet_wrap(~species,nrow=1)+
#   xlab("Grazing Intensity") +
#   ylab("Predicted Probability of Occurrence") +
#   ggsave("images/binomial_preds_grazing_intensity.png", width=7, height=3)
# 
# 
# pred_df %>%
#   filter(fg != "NC" & fg != "IW" & fg != "IPG") %>%
#   ggplot(aes(x=grazing_intensity, y= mean, group=species,color = origin)) +
#   geom_line(lwd=1, alpha=0.5)+
#   facet_wrap(~fg) +
#   xlab("Grazing Intensity") +
#   ylab("Probability of Occurrence") +
#   theme(legend.position = c(1,.45),
#         legend.justification = c(1,1))+
#   ggsave("images/fg_preds_grazing_intensity_binomial.png", height=7, width=7) 
# 
# ggplot(pred_df, aes(x=grazing_intensity, y= mean, 
#                     group=species,color = origin)) +
#   geom_line(lwd=1, alpha=0.5)+
#   geom_line(lty=2, aes(y=upr))+
#   geom_line(lty=2, aes(y=lwr)) +
#   facet_wrap(~species)+
#   theme(axis.text=element_blank())

# from sb ============
n_runs <- nChains*samples


pred_df <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(grazing_intensity = rep(Gradient$XDataNew$grazing_intensity, n_runs)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -grazing_intensity) %>%
  group_by(Species, grazing_intensity) %>%
  dplyr::summarise(mean = median(cover),
                   upr = quantile(cover, 0.975),
                   lwr = quantile(cover, 0.025)) %>%
  ungroup() %>%
  # mutate(Species = str_replace_all(Species," ", "_"),
  #        Species = lut_prevalent_species[Species]) %>%
  left_join(prevalence) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  filter(origin != "IPG" & origin != "IW") %>%
  arrange(origin,desc(prevalence)) %>%
  filter(prevalence>4) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species))) %>%
  filter(Species != "unknown_forb") 

pred_df_raw <- do.call("rbind", predY) %>%
  as_tibble() %>%
  mutate(grazing_intensity = rep(Gradient$XDataNew$grazing_intensity, n_runs),
         run = rep(1:n_runs,each=20)) %>%
  pivot_longer(values_to = "cover", names_to = "Species", -c(grazing_intensity,run))%>%
  # mutate(Species = str_replace_all(Species," ", "_")) %>%
  left_join(prevalence) %>%
  filter(prevalence>4) %>%
  mutate(origin = lut_all_fg[Species]) %>%
  arrange(origin,desc(prevalence)) %>%
  filter(Species != "unknown_forb")%>%
  # mutate(Species = lut_prevalent_species[Species]) %>%
  mutate(Species_f = factor(Species, levels = unique(.$Species)))

pred_df_raw  %>%
  filter(origin != "IPG" & origin != "IW") %>%
  mutate(origin = c("IAF" = "Introduced Annual Forb",
                    "IAG" = "Introduced Annual Grass",
                    "NAF" = "Native Annual Forb",
                    "NPF" = "Native Perennial Forb",
                    "NPG" = "Native Perennial Grass",
                    "NW" = "Native Shrub")[origin])%>%
  ggplot(aes(x=grazing_intensity, y=cover)) +
  geom_line(alpha=0.01, aes(group=run, color = origin), key_glyph="rect")+
  geom_line(data = pred_df,lwd=1, alpha=0.95, color="black", aes(y=mean))+
  facet_wrap(~Species_f, nrow=4)+
  xlab("Grazing Intensity (AUM/ha)") +
  ylab("Probability of Occurrence") +
  guides(color=guide_legend(override.aes = list(alpha=1)))+
  scale_color_brewer(palette = "Dark2") +
  # scale_color_manual(values = c("#FFC845", "#007DBA"), 
  #                    labels = c("Introduced", "Native"))+
  theme(legend.position = "right",
        # legend.justification = c(1,0),
        legend.title = element_blank())+
  ggsave("images/probit_preds_grazing_intensity_byspp_burned.png", width=13.5, height=9.5)
